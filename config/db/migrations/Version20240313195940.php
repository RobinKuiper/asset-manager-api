<?php

declare(strict_types = 1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240313195940 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Initial setup';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE log_entries (id INT AUTO_INCREMENT NOT NULL, action VARCHAR(8) NOT NULL, logged_at DATETIME NOT NULL, object_id VARCHAR(64) DEFAULT NULL, object_class VARCHAR(191) NOT NULL, version INT NOT NULL, data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', username VARCHAR(191) DEFAULT NULL, INDEX log_class_lookup_idx (object_class), INDEX log_date_lookup_idx (logged_at), INDEX log_user_lookup_idx (username), INDEX log_version_lookup_idx (object_id, object_class, version), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB ROW_FORMAT = DYNAMIC');
        $this->addSql('CREATE TABLE permissions (id INT UNSIGNED AUTO_INCREMENT NOT NULL, created_by INT UNSIGNED DEFAULT NULL, updated_by INT UNSIGNED DEFAULT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_2DEDCC6FDE12AB56 (created_by), INDEX IDX_2DEDCC6F16FE72E1 (updated_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE refresh_tokens (id INT UNSIGNED AUTO_INCREMENT NOT NULL, user_id INT UNSIGNED DEFAULT NULL, token VARCHAR(255) NOT NULL, expiry_date DATETIME NOT NULL, INDEX IDX_9BACE7E1A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE roles (id INT UNSIGNED AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role_permission (role_id INT UNSIGNED NOT NULL, permission_id INT UNSIGNED NOT NULL, INDEX IDX_6F7DF886D60322AC (role_id), INDEX IDX_6F7DF886FED90CCA (permission_id), PRIMARY KEY(role_id, permission_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT UNSIGNED AUTO_INCREMENT NOT NULL, role_id INT UNSIGNED DEFAULT NULL, created_by INT UNSIGNED DEFAULT NULL, updated_by INT UNSIGNED DEFAULT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_1483A5E9D60322AC (role_id), INDEX IDX_1483A5E9DE12AB56 (created_by), INDEX IDX_1483A5E916FE72E1 (updated_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_permission (user_id INT UNSIGNED NOT NULL, permission_id INT UNSIGNED NOT NULL, INDEX IDX_472E5446A76ED395 (user_id), INDEX IDX_472E5446FED90CCA (permission_id), PRIMARY KEY(user_id, permission_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE permissions ADD CONSTRAINT FK_2DEDCC6FDE12AB56 FOREIGN KEY (created_by) REFERENCES users (id)');
        $this->addSql('ALTER TABLE permissions ADD CONSTRAINT FK_2DEDCC6F16FE72E1 FOREIGN KEY (updated_by) REFERENCES users (id)');
        $this->addSql('ALTER TABLE refresh_tokens ADD CONSTRAINT FK_9BACE7E1A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE role_permission ADD CONSTRAINT FK_6F7DF886D60322AC FOREIGN KEY (role_id) REFERENCES roles (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE role_permission ADD CONSTRAINT FK_6F7DF886FED90CCA FOREIGN KEY (permission_id) REFERENCES permissions (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E9D60322AC FOREIGN KEY (role_id) REFERENCES roles (id)');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E9DE12AB56 FOREIGN KEY (created_by) REFERENCES users (id)');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E916FE72E1 FOREIGN KEY (updated_by) REFERENCES users (id)');
        $this->addSql('ALTER TABLE user_permission ADD CONSTRAINT FK_472E5446A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_permission ADD CONSTRAINT FK_472E5446FED90CCA FOREIGN KEY (permission_id) REFERENCES permissions (id) ON DELETE CASCADE');

        $date = date('Y-m-d H:i:s');
        $this->addDefaultRoles($date);
        $this->addDefaultPermissions($date);
        $this->addDefaultRolePermissions();
    }

    private function addDefaultRoles(string $date): void
    {
        $defaultRoles = ['User', 'Admin'];

        foreach ($defaultRoles as $name) {
            $this->addSql("INSERT INTO roles (name, created_at, updated_at) VALUES ('$name', '$date', '$date')");
        }
    }

    private function addDefaultPermissions(string $date): void
    {
        $defaultPermissions = [
            ['name' => 'user.get.all', 'description' => 'Get all users'],
            ['name' => 'user.create', 'description' => 'Create users'],
            ['name' => 'user.update', 'description' => 'Update users'],
            ['name' => 'user.delete', 'description' => 'Delete users'],

            ['name' => 'role.get.all', 'description' => 'Get all roles'],
            ['name' => 'role.create', 'description' => 'Create roles'],
            ['name' => 'role.update', 'description' => 'Update roles'],
            ['name' => 'role.delete', 'description' => 'Delete roles'],

            ['name' => 'permission.get.all', 'description' => 'Get all permissions'],
            ['name' => 'permission.create', 'description' => 'Create permissions'],
            ['name' => 'permission.update', 'description' => 'Update permissions'],
            ['name' => 'permission.delete', 'description' => 'Delete permissions'],

            ['name' => 'log.get.all', 'description' => 'Get all logs'],
        ];

        foreach ($defaultPermissions as $permission) {
            ['name' => $name, 'description' => $description] = $permission;
            $this->addSql("INSERT INTO permissions (name, description, created_at, updated_at, created_by, updated_by) VALUES ('$name', '$description', '$date', '$date', null, null)");
        }
    }

    private function addDefaultRolePermissions(): void
    {
        $defaultRolePermissions = [
            // Insert permissions for "User"
            //            ['roleId' => 1, 'permissionId' => 1],
            //            ['roleId' => 1, 'permissionId' => 2],
            //            ['roleId' => 1, 'permissionId' => 3],
            //            ['roleId' => 1, 'permissionId' => 4],

            // Insert permissions for "Admin"
            ['roleId' => 2, 'permissionId' => 1],
            ['roleId' => 2, 'permissionId' => 2],
            ['roleId' => 2, 'permissionId' => 3],
            ['roleId' => 2, 'permissionId' => 4],
            ['roleId' => 2, 'permissionId' => 5],
            ['roleId' => 2, 'permissionId' => 6],
            ['roleId' => 2, 'permissionId' => 7],
            ['roleId' => 2, 'permissionId' => 8],
            ['roleId' => 2, 'permissionId' => 9],
            ['roleId' => 2, 'permissionId' => 10],
            ['roleId' => 2, 'permissionId' => 11],
            ['roleId' => 2, 'permissionId' => 12],
            ['roleId' => 2, 'permissionId' => 13],
        ];

        foreach ($defaultRolePermissions as $rolePermission) {
            ['roleId' => $roleId, 'permissionId' => $permissionId] = $rolePermission;
            $this->addSql("INSERT INTO role_permission (role_id, permission_id) VALUES ($roleId, $permissionId)");
        }
    }

    public function down(Schema $schema): void
    {
        $this->addSql('TRUNCATE TABLE role_permission');
        $this->addSql('TRUNCATE TABLE permissions');
        $this->addSql('TRUNCATE TABLE roles');

        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE permissions DROP FOREIGN KEY FK_2DEDCC6FDE12AB56');
        $this->addSql('ALTER TABLE permissions DROP FOREIGN KEY FK_2DEDCC6F16FE72E1');
        $this->addSql('ALTER TABLE refresh_tokens DROP FOREIGN KEY FK_9BACE7E1A76ED395');
        $this->addSql('ALTER TABLE role_permission DROP FOREIGN KEY FK_6F7DF886D60322AC');
        $this->addSql('ALTER TABLE role_permission DROP FOREIGN KEY FK_6F7DF886FED90CCA');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E9D60322AC');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E9DE12AB56');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E916FE72E1');
        $this->addSql('ALTER TABLE user_permission DROP FOREIGN KEY FK_472E5446A76ED395');
        $this->addSql('ALTER TABLE user_permission DROP FOREIGN KEY FK_472E5446FED90CCA');
        $this->addSql('DROP TABLE log_entries');
        $this->addSql('DROP TABLE permissions');
        $this->addSql('DROP TABLE refresh_tokens');
        $this->addSql('DROP TABLE roles');
        $this->addSql('DROP TABLE role_permission');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE user_permission');
    }
}
