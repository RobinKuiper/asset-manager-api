<?php

use App\Controllers\PermissionController;
use App\Interfaces\IEntityManagerService;
use App\Middleware\PermissionMiddleware;
use App\Middleware\ValidatorMiddleware;
use App\Validators\PermissionRequestValidator;
use DI\NotFoundException;
use Slim\App;
use Slim\Routing\RouteCollectorProxy;

return function (RouteCollectorProxy $group, App $app) {
    $group->group('/permissions', function (RouteCollectorProxy $permissions) use ($app) {
        $container = $app->getContainer();

        if (is_null($container)) {
            throw new NotFoundException('Container not found');
        }

        $permissions->get('', [PermissionController::class, 'getAll'])
            ->add(new PermissionMiddleware(['permission.get.all']));
        $permissions->get('/paginated', [PermissionController::class, 'getPaginated'])
            ->add(new PermissionMiddleware(['permission.get.all']));
        $permissions->post('', [PermissionController::class, 'create'])
            ->add(new ValidatorMiddleware(
                new PermissionRequestValidator($container->get(IEntityManagerService::class))
            ))
            ->add(new PermissionMiddleware(['permission.create']));

        $permissions->group('/{permission}', function (RouteCollectorProxy $permission) use ($container) {
            $permission->get('', [PermissionController::class, 'get'])
                ->add(new PermissionMiddleware(['permission.get.all']));
            $permission->get('/logs', [PermissionController::class, 'getLogs'])
                ->add(new PermissionMiddleware(['log.get.all']));
            $permission->delete('', [PermissionController::class, 'delete'])
                ->add(new PermissionMiddleware(['permission.delete']));

            $permission->group('', function (RouteCollectorProxy $permissionWithData) {
                $permissionWithData->put('', [PermissionController::class, 'update']);
                $permissionWithData->patch('', [PermissionController::class, 'patch']);
            })->add(new ValidatorMiddleware(
                new PermissionRequestValidator($container->get(IEntityManagerService::class))
            ))->add(new PermissionMiddleware(['permission.update']));
        });
    });
};
