<?php

use App\Controllers\UserProviderController;
use App\Interfaces\IEntityManagerService;
use App\Middleware\PermissionMiddleware;
use App\Middleware\ValidatorMiddleware;
use App\Validators\AddPermissionRequestValidator;
use App\Validators\UserRequestValidator;
use DI\NotFoundException;
use Slim\App;
use Slim\Routing\RouteCollectorProxy;

return function (RouteCollectorProxy $group, App $app) {
    $group->group('/users', function (RouteCollectorProxy $users) use ($app) {
        $container = $app->getContainer();

        if (is_null($container)) {
            throw new NotFoundException('Container not found');
        }

        $users->get('', [UserProviderController::class, 'getAll'])
            ->add(new PermissionMiddleware(['user.get.all']));
        $users->get('/paginated', [UserProviderController::class, 'getPaginated'])
            ->add(new PermissionMiddleware(['user.get.all']));
        $users->post('', [UserProviderController::class, 'create'])
            ->add(new ValidatorMiddleware(
                new UserRequestValidator($container->get(IEntityManagerService::class))
            ))
            ->add(new PermissionMiddleware(['user.create']));

        $users->group('/{user}', function (RouteCollectorProxy $user) use ($container) {
            $user->get('', [UserProviderController::class, 'get'])
                ->add(new PermissionMiddleware(['user.get.all']));
            $user->patch('', [UserProviderController::class, 'patch'])
                ->add(new ValidatorMiddleware(
                    new UserRequestValidator($container->get(IEntityManagerService::class))
                ))
                ->add(new PermissionMiddleware(['user.update']));
            $user->get('/logs', [UserProviderController::class, 'getLogs'])
                ->add(new PermissionMiddleware(['log.get.all']));
            $user->delete('', [UserProviderController::class, 'delete'])
                ->add(new PermissionMiddleware(['user.delete']));

            $user->group('/permission', function (RouteCollectorProxy $permission) use ($container) {
                $permission->post('', [UserProviderController::class, 'addPermission'])
                    ->add(new ValidatorMiddleware(
                        new AddPermissionRequestValidator($container->get(IEntityManagerService::class))
                    ))
                    ->add(new PermissionMiddleware(['user.create'])); // TODO: Check permission
                $permission->delete('/{permission}', [UserProviderController::class, 'removePermission'])
                  ->add(new PermissionMiddleware(['user.create'])); // TODO: Check permission
            });
        });
    });
};
