<?php

use App\Controllers\RoleController;
use App\Interfaces\IEntityManagerService;
use App\Middleware\PermissionMiddleware;
use App\Middleware\ValidatorMiddleware;
use App\Validators\AddPermissionRequestValidator;
use App\Validators\RoleRequestValidator;
use DI\NotFoundException;
use Slim\App;
use Slim\Routing\RouteCollectorProxy;

return function (RouteCollectorProxy $group, App $app) {
    $group->group('/roles', function (RouteCollectorProxy $roles) use ($app) {
        $container = $app->getContainer();

        if (is_null($container)) {
            throw new NotFoundException('Container not found');
        }

        $roles->get('', [RoleController::class, 'getAll'])
            ->add(new PermissionMiddleware(['role.get.all']));
        $roles->get('/paginated', [RoleController::class, 'getPaginated'])
            ->add(new PermissionMiddleware(['role.get.all']));
        $roles->post('', [RoleController::class, 'create'])
            ->add(new ValidatorMiddleware(
                new RoleRequestValidator($container->get(IEntityManagerService::class))
            ))
            ->add(new PermissionMiddleware(['role.create']));

        $roles->group('/{role}', function (RouteCollectorProxy $role) use ($container) {
            $role->get('', [RoleController::class, 'get'])
                ->add(new PermissionMiddleware(['role.get.all']));
            $role->get('/logs', [RoleController::class, 'getLogs'])
                ->add(new PermissionMiddleware(['log.get.all']));
            $role->delete('', [RoleController::class, 'delete'])
                ->add(new PermissionMiddleware(['role.delete']));

            $role->group('', function (RouteCollectorProxy $roleWithData) {
                $roleWithData->put('', [RoleController::class, 'update']);
                $roleWithData->patch('', [RoleController::class, 'patch']);
            })->add(new ValidatorMiddleware(
                new RoleRequestValidator($container->get(IEntityManagerService::class))
            ))->add(new PermissionMiddleware(['role.update']));

            $role->group('/permission', function (RouteCollectorProxy $permission) use ($container) {
                $permission->post('', [RoleController::class, 'addPermission'])
                    ->add(new ValidatorMiddleware(
                        new AddPermissionRequestValidator($container->get(IEntityManagerService::class))
                    ))
                    ->add(new PermissionMiddleware(['permission.create'])); // TODO: Check permission
                $permission->delete('/{permission}', [RoleController::class, 'removePermission'])
                  ->add(new PermissionMiddleware(['permission.delete'])); // TODO: Check permission
            });
        });
    });
};
