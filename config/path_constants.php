<?php

declare(strict_types = 1);

const APP_PATH     = __DIR__ . '/../src';
const CONFIG_PATH  = __DIR__;
const STORAGE_PATH = __DIR__ . '/../storage';
const CACHE_PATH = STORAGE_PATH . '/cache';
