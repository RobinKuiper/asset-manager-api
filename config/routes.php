<?php

declare(strict_types = 1);

use App\Controllers\AuthController;
use App\Interfaces\IEntityManagerService;
use App\Middleware\AuthMiddleware;
use App\Middleware\ValidatorMiddleware;
use App\Validators\LoginUserRequestValidator;
use App\Validators\RegisterUserRequestValidator;
use DI\NotFoundException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Exception\HttpNotFoundException;
use Slim\Routing\RouteCollectorProxy;

return function (App $app) {
    $app->get('/', function (Response $response) {
        $response->getBody()->write('Nothing here!');

        return $response;
    });

    $container = $app->getContainer();

    if (is_null($container)) {
        throw new NotFoundException('Container not found');
    }

    $app->group('/api', function (RouteCollectorProxy $api) use ($container) {
        $api->group('/v1', function (RouteCollectorProxy $v1) use ($container) {
            $v1->group('/auth', function (RouteCollectorProxy $auth) use ($container) {
                $auth->post('/login', [AuthController::class, 'logIn'])
                    ->add(new ValidatorMiddleware(new LoginUserRequestValidator()));
                $auth->post('/register', [AuthController::class, 'register'])
                    ->add(new ValidatorMiddleware(
                        new RegisterUserRequestValidator($container->get(IEntityManagerService::class))
                    ));
                $auth->post('/token-refresh', [AuthController::class, 'tokenRenewal'])
                    ->add(AuthMiddleware::class)->setName('tokenRenewal');
            });
        });
    });

    $app->group('/api', function (RouteCollectorProxy $api) use ($app) {
        $api->group('/v1', function (RouteCollectorProxy $v1) use ($app) {
            foreach ((array)glob(CONFIG_PATH . '/routes/v1/*.php') as $file) {
                $routeFile = require $file;
                $routeFile($v1, $app);
            }
        });
    })->add(AuthMiddleware::class);

    $app->options('/{routes:.+}', function (Response $response) {
        return $response;
    });

    $app->map(
        ['GET', 'POST', 'PUT', 'DELETE', 'PATCH'],
        '/{routes:.+}',
        function (Request $request) {
            throw new HttpNotFoundException($request);
        }
    );
};
