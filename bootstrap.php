<?php

declare(strict_types = 1);

use Dotenv\Dotenv;
use Dotenv\Exception\InvalidEncodingException;
use Dotenv\Exception\InvalidFileException;
use Dotenv\Exception\InvalidPathException;

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/config/constants.php';

// Load environment vars if we are not in production
if (!isset($_ENV['APP_ENV']) || $_ENV['APP_ENV'] !== 'production') {
    $dotenv = Dotenv::createImmutable(__DIR__);
    try {
        $dotenv->load();
    } catch (InvalidEncodingException|InvalidFileException|InvalidPathException $e) {
        echo ".env file not found\n" . PHP_EOL;
        echo "Please copy the .env.example in the root to .env and set the values before trying again." . PHP_EOL;
        throw new RuntimeException(".env file not found");
    }
}

return require_once CONFIG_PATH . '/container.php';
