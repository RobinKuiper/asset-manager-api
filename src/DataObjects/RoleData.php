<?php

declare(strict_types = 1);

namespace App\DataObjects;

readonly class RoleData
{
    public function __construct(
        public string $name,
    ) {
    }
}
