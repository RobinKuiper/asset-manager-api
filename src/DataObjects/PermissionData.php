<?php

declare(strict_types = 1);

namespace App\DataObjects;

readonly class PermissionData
{
    public function __construct(
        public string $name,
        public string $description,
    ) {
    }
}
