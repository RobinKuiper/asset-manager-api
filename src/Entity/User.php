<?php

declare(strict_types = 1);

namespace App\Entity;

use App\Entity\Traits\HasTimestamps;
use App\Entity\Traits\IsBlameable;
use App\Interfaces\IUser;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;
use Gedmo\Loggable\Loggable;
use Gedmo\Mapping\Annotation as Gedmo;
use JsonSerializable;

#[Entity, Table('users')]
#[HasLifecycleCallbacks]
#[Gedmo\Loggable(logEntryClass: LogEntry::class)]
class User implements IUser, JsonSerializable, Loggable
{
    use HasTimestamps;
    use IsBlameable;

    #[Id, Column(options: ['unsigned' => true]), GeneratedValue]
    private int $id;

    #[Gedmo\Versioned]
    #[Column]
    private string $name;

    #[Gedmo\Versioned]
    #[Column]
    private string $email;

    #[Gedmo\Versioned]
    #[Column]
    private string $password;

    #[Gedmo\Versioned]
    #[ManyToOne(targetEntity: Role::class, inversedBy: 'users')]
    private Role $role;

    /**
     * @var Collection<int, Permission> $permissions
     */
    #[ManyToMany(targetEntity: Permission::class, inversedBy: 'users')]
    private Collection $permissions;

    /**
     * @var Collection<int, RefreshToken> $refreshTokens
     */
    #[OneToMany(mappedBy: 'user', targetEntity: RefreshToken::class)]
    private Collection $refreshTokens;

    public function __construct()
    {
        $this->permissions = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): User
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): User
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): User
    {
        $this->password = $password;

        return $this;
    }

    public function getRole(): Role
    {
        return $this->role;
    }

    public function setRole(Role $role): void
    {
        $this->role = $role;
    }

    /**
     * @return Collection<int, Permission>
     */
    public function getPermissions(): Collection
    {
        return $this->permissions;
    }

    public function addPermission(Permission $permission): void
    {
        $this->permissions[] = $permission;
    }

    public function removePermission(Permission $permission): void
    {
        $this->permissions->removeElement($permission);
    }

    /**
     * @return Collection<int, RefreshToken>
     */
    public function getRefreshTokens(): Collection
    {
        return $this->refreshTokens;
    }

    public function addRefreshToken(RefreshToken $refreshToken): void
    {
        $this->refreshTokens[] = $refreshToken;
    }

    public function removeRefreshToken(RefreshToken $refreshToken): void
    {
        $this->refreshTokens->removeElement($refreshToken);
    }

    public function jsonSerialize(): array
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'email'       => $this->email,
            'role'        => [
                'id'        => $this->role->getId(),
                'name'      => $this->role->getName(),
                'createdAt' => $this->role->getCreatedAt()->getTimestamp(),
                'updatedAt' => $this->role->getUpdatedAt()->getTimestamp(),
            ],
            'permissions' => $this->permissions->map(fn (Permission $permission) => [
                'id'           => $permission->getId(),
                'name'         => $permission->getName(),
                'description'  => $permission->getDescription(),
                'createdAt'    => $permission->getCreatedAt()->getTimestamp(),
                'updatedAt'    => $permission->getUpdatedAt()->getTimestamp(),
            ])->toArray(),
            'createdAt'   => $this->createdAt->getTimestamp(),
            'updatedAt'   => $this->updatedAt->getTimestamp(),
        ];
    }
}
