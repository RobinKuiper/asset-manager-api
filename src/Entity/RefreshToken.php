<?php

declare(strict_types = 1);

namespace App\Entity;

use App\Interfaces\IUser;
use DateInterval;
use DateTime;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Ramsey\Uuid\Uuid;

#[Entity, Table('refresh_tokens')]
class RefreshToken
{
    #[Id, Column(options: ['unsigned' => true]), GeneratedValue]
    private int $id;

    #[ManyToOne(targetEntity: User::class, inversedBy: 'refreshTokens')]
    private IUser $user;

    #[Column]
    private string $token;

    #[Column(name: 'expiry_date')]
    private DateTime $expiryDate;

    public function __construct()
    {
        $this->token      = Uuid::uuid4()->toString();
        $this->expiryDate = new DateTime();
        $this->expiryDate->add(new DateInterval('PT24H'));
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUser(): IUser
    {
        return $this->user;
    }

    public function setUser(IUser $user): void
    {
        $this->user = $user;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getExpiryDate(): DateTime
    {
        return $this->expiryDate;
    }
}
