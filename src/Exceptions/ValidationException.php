<?php

declare(strict_types = 1);

namespace App\Exceptions;

use App\Enum\StatusCode;
use RuntimeException;
use Throwable;

class ValidationException extends RuntimeException
{
    public function __construct(
        public array|string $errors,
        string $message = 'Validation Error(s)',
        int $code = StatusCode::UnprocessableEntity->value,
        ?Throwable $previous = null
    ) {
        if (is_string($errors)) {
            $this->errors = [$errors];
        }

        parent::__construct($message, $code, $previous);
    }
}
