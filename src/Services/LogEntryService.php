<?php

declare(strict_types = 1);

namespace App\Services;

use App\DataObjects\DataTableQueryParams;
use App\Entity\LogEntry;
use App\Interfaces\IEntityManagerService;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class LogEntryService
{
    public function __construct(protected IEntityManagerService $entityManager)
    {
    }

    /**
     * Retrieves paginated results based on the provided DataTableQueryParams.
     *
     * @param DataTableQueryParams $params the query parameters for pagination
     *
     * @return Paginator the paginated results
     */
    public function getPaginatedResults(DataTableQueryParams $params): Paginator
    {
        $objectRepository = $this->entityManager->getRepository(LogEntry::class);
        assert($objectRepository instanceof EntityRepository);
        $repository = $objectRepository;

        $query = $repository
            ->createQueryBuilder('p')
            ->setFirstResult($params->start)
            ->setMaxResults($params->length);

        $orderBy  = in_array($params->orderBy, ['action', 'loggedAt', 'objectId', 'version', 'username', 'createdAt', 'updatedAt'], true) ? $params->orderBy : 'id';
        $orderDir = strtolower($params->orderDir) === 'desc' ? 'desc' : 'asc';

        if (! empty($params->searchTerm)) {
            $query->where('p.action LIKE :action')->setParameter(
                'action',
                '%' . addcslashes($params->searchTerm, '%_') . '%'
            );
            $query->orWhere('p.username LIKE :username')->setParameter(
                'username',
                '%' . addcslashes($params->searchTerm, '%_') . '%'
            );
            $query->orWhere('p.objectClass LIKE :objectClass')->setParameter(
                'objectClass',
                '%' . addcslashes($params->searchTerm, '%_') . '%'
            );
        }

        $query->orderBy('p.' . $orderBy, $orderDir);

        return new Paginator($query);
    }
}
