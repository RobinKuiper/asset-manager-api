<?php

declare(strict_types = 1);

namespace App\Services;

use App\Interfaces\IUser;

class AuthenticationService
{
    /**
     * Attempts to log in a user with the given credentials.
     *
     * @param IUser $user
     * @param array $credentials the user credentials
     *
     * @return bool
     */
    public function attemptLogin(IUser $user, array $credentials): bool
    {
        return password_verify($credentials['password'], $user->getPassword());
    }
}
