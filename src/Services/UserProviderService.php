<?php

declare(strict_types = 1);

namespace App\Services;

use App\DataObjects\DataTableQueryParams;
use App\DataObjects\RegisterUserData;
use App\Entity\Permission;
use App\Entity\Role;
use App\Entity\User;
use App\Interfaces\IEntityManagerService;
use App\Interfaces\IUser;
use App\Interfaces\IUserProviderService;
use DI\NotFoundException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class UserProviderService implements IUserProviderService
{
    /**
     * @var EntityRepository<User>
     */
    protected EntityRepository $repository;

    public function __construct(
        protected IEntityManagerService $entityManager,
        readonly private HashService $hashService
    ) {
        $objectRepository = $this->entityManager->getRepository(User::class);
        assert($objectRepository instanceof EntityRepository);
        $this->repository = $objectRepository;
    }

    /**
     * Get user by id.
     *
     * @param int $userId the user id
     *
     * @return IUser|null the user object if found, null otherwise
     */
    public function getById(int $userId): ?IUser
    {
        return $this->entityManager->find(User::class, $userId);
    }

    /**
     * Retrieves a user by their credentials.
     *
     * @param array $credentials an array containing the user's credentials, such as email
     *
     * @return IUser|null the user object if found, or null if not found
     */
    public function getByCredentials(array $credentials): ?IUser
    {
        return $this->repository->findOneBy(['email' => $credentials['email']]);
    }

    /**
     * Creates a new User object based on the provided data.
     *
     * @param RegisterUserData $data the registration data for the new user
     *
     * @throws NotFoundException
     *
     * @return IUser the newly created User object
     */
    public function createUser(RegisterUserData $data): IUser
    {
        $role = !is_null($data->role) ? $data->role : $this->entityManager->find(Role::class, 1);

        if (is_null($role)) {
            throw new NotFoundException('Role not found');
        }

        $user = new User();

        $user->setName($data->name);
        $user->setEmail($data->email);
        $user->setPassword($this->hashService->hashPassword($data->password));
        $user->setRole($role);

        $this->entityManager->sync($user);

        return $user;
    }

    /**
     * Updates a role with the provided data.
     *
     * @param User $user the role to be updated
     * @param array $data the data used to update the role
     *
     * @return User the updated role
     */
    public function patch(User $user, array $data): User
    {
        if (isset($data['name'])) {
            $user->setName($data['name']);
        }

        if (isset($data['email'])) {
            $user->setEmail($data['email']);
        }

        if (isset($data['role'])) {
            $user->setRole($data['role']);
        }

        $this->entityManager->sync($user);

        return $user;
    }

    /**
     * Updates the password for a user.
     *
     * @param IUser $user the user object to update the password for
     * @param string $password the new password string
     *
     * @return IUser the updated user object
     */
    public function updatePassword(IUser $user, string $password): IUser
    {
        $user->setPassword($this->hashService->hashPassword($password));

        $this->entityManager->sync($user);

        return $user;
    }

    /**
     * Adds a permission to a user.
     *
     * @param IUser $user the user object to add the permission to
     * @param Permission $permission the permission object to be added
     *
     * @return IUser the updated user object with the added permission
     */
    public function addPermission(IUser $user, Permission $permission): IUser
    {
        $user->addPermission($permission);

        $this->entityManager->sync($user);

        return $user;
    }

    /**
     * Removes a permission from a user.
     *
     * @param IUser $user the user object from which the permission will be removed
     * @param Permission $permission the permission object to be removed
     *
     * @return IUser the updated user object
     */
    public function removePermission(IUser $user, Permission $permission): IUser
    {
        $user->removePermission($permission);

        $this->entityManager->sync($user);

        return $user;
    }

    /**
     * Retrieves paginated results based on the provided DataTableQueryParams.
     *
     * @param DataTableQueryParams $params the query parameters for pagination
     *
     * @return Paginator the paginated results
     */
    public function getPaginatedResults(DataTableQueryParams $params): Paginator
    {
        $query = $this->repository
            ->createQueryBuilder('u')
            ->setFirstResult($params->start)
            ->setMaxResults($params->length);

        $orderBy  = in_array($params->orderBy, ['name', 'email', 'createdAt', 'updatedAt'], true) ? $params->orderBy : 'id';
        $orderDir = strtolower($params->orderDir) === 'desc' ? 'desc' : 'asc';

        if (! empty($params->searchTerm)) {
            $query->where('u.name LIKE :name')->setParameter(
                'name',
                '%' . addcslashes($params->searchTerm, '%_') . '%'
            );
            $query->orWhere('u.email LIKE :email')->setParameter(
                'email',
                '%' . addcslashes($params->searchTerm, '%_') . '%'
            );
        }

        $query->orderBy('u.' . $orderBy, $orderDir);

        return new Paginator($query);
    }
}
