<?php

declare(strict_types = 1);

namespace App\Services;

use App\Config;
use App\DataObjects\JwtTokenUserData;
use App\Entity\Permission;
use App\Entity\RefreshToken;
use App\Interfaces\IEntityManagerService;
use App\Interfaces\IUser;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityRepository;
use Exception;
use Firebase\JWT\JWT;

class TokenService
{
    /**
     * @var EntityRepository<RefreshToken>
     */
    protected EntityRepository $refreshTokenRepository;

    public function __construct(
        protected IEntityManagerService $entityManager,
        readonly private Config $config
    ) {
        $objectRepository = $this->entityManager->getRepository(RefreshToken::class);
        assert($objectRepository instanceof EntityRepository);
        $this->refreshTokenRepository = $objectRepository;
    }

    /**
     * @param IUser $user
     *
     * @throws Exception
     *
     * @return array
     */
    public function generateTokens(IUser $user): array
    {
        $refreshToken = $this->generateRefreshToken($user);
        $jwtToken     = $this->generateJwtToken($user);

        return [$jwtToken, $refreshToken];
    }

    /**
     * @param IUser $user
     *
     * @return string
     */
    public function generateRefreshToken(IUser $user): string
    {
        $refreshToken = new RefreshToken();
        $refreshToken->setUser($user);
        $this->entityManager->sync($refreshToken);

        return $refreshToken->getToken();
    }

    /**
     * Generates a JWT token for the given user.
     *
     * @param IUser $user the user object
     *
     * @throws Exception
     *
     * @return string the generated JWT token
     */
    public function generateJwtToken(IUser $user): string
    {
        $expirationTimeInSeconds = $this->config->get('jwt_authentication.expiration_time');
        $expire    = (new DateTime('now', new DateTimeZone('Europe/Amsterdam')))
            ->modify("+$expirationTimeInSeconds seconds")->getTimestamp();
        $secret    = $this->config->get('jwt_authentication.secret');
        $algorithm = $this->config->get('jwt_authentication.algorithm');

        $userData = new JwtTokenUserData(
            $user->getName(),
            $user->getEmail(),
            $user->getRole()->getName(),
            array_merge(
                $user->getRole()->getPermissions()->map(fn (Permission $permission) => $permission->getName())->toArray(),
                $user->getPermissions()->map(fn (Permission $permission) => $permission->getName())->toArray(),
            )
        );

        return JWT::encode([
            'exp'  => $expire,
            'iat'  => (new DateTime('now'))->getTimestamp(),
            'user' => $userData,
        ], $secret, $algorithm);
    }

    /**
     * @param IUser $user
     * @param string $refreshToken
     *
     * @return RefreshToken|false
     */
    public function getRefreshToken(IUser $user, string $refreshToken): RefreshToken|false
    {
        $refreshToken =  $this->refreshTokenRepository->findOneBy([
            'user'  => $user,
            'token' => $refreshToken,
        ]);

        return $refreshToken ?? false;
    }

    /**
     * @param RefreshToken $refreshToken
     *
     * @return void
     */
    public function removeRefreshToken(RefreshToken $refreshToken): void
    {
        $this->entityManager->remove($refreshToken);
        $this->entityManager->flush();
    }
}
