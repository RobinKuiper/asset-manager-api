<?php

declare(strict_types = 1);

namespace App\Services;

use App\DataObjects\DataTableQueryParams;
use App\DataObjects\PermissionData;
use App\Entity\Permission;
use App\Interfaces\IEntityManagerService;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class PermissionService
{
    public function __construct(protected IEntityManagerService $entityManager)
    {
    }

    /**
     * Creates a new product based on the provided data.
     *
     * @param PermissionData $data the data used to create the product
     *
     * @return Permission the created product
     */
    public function create(PermissionData $data): Permission
    {
        $permission = new Permission();

        return $this->update(
            $permission,
            $data
        );
    }

    /**
     * Updates the details of a product based on the provided data.
     *
     * @param Permission           $permission the product to update
     * @param PermissionData $data    the updated data for the product
     *
     * @return Permission the updated product
     */
    public function update(Permission $permission, PermissionData $data): Permission
    {
        $permission->setName($data->name);
        $permission->setDescription($data->description);

        $this->entityManager->sync($permission);

        return $permission;
    }

    /**
     * Updates the fields of a given permission based on the provided data.
     *
     * @param Permission $permission the permission object to update
     * @param array $data the data used to update the permission
     *
     * @return Permission the updated permission object
     */
    public function patch(Permission $permission, array $data): Permission
    {
        if (isset($data['name'])) {
            $permission->setName($data['name']);
        }

        if (isset($data['description'])) {
            $permission->setDescription($data['description']);
        }

        $this->entityManager->sync($permission);

        return $permission;
    }

    /**
     * Retrieves paginated results based on the provided DataTableQueryParams.
     *
     * @param DataTableQueryParams $params the query parameters for pagination
     *
     * @return Paginator the paginated results
     */
    public function getPaginatedResults(DataTableQueryParams $params): Paginator
    {
        $objectRepository = $this->entityManager->getRepository(Permission::class);
        assert($objectRepository instanceof EntityRepository);
        $repository = $objectRepository;

        $query = $repository
            ->createQueryBuilder('p')
            ->setFirstResult($params->start)
            ->setMaxResults($params->length);

        $orderBy  = in_array($params->orderBy, ['name', 'description', 'createdAt', 'updatedAt'], true) ? $params->orderBy : 'id';
        $orderDir = strtolower($params->orderDir) === 'desc' ? 'desc' : 'asc';

        if (! empty($params->searchTerm)) {
            $query->where('p.name LIKE :name')->setParameter(
                'name',
                '%' . addcslashes($params->searchTerm, '%_') . '%'
            );
            $query->andWhere('p.description LIKE :description')->setParameter(
                'description',
                '%' . addcslashes($params->searchTerm, '%_') . '%'
            );
        }

        $query->orderBy('p.' . $orderBy, $orderDir);

        return new Paginator($query);
    }
}
