<?php

declare(strict_types = 1);

namespace App\Controllers;

use App\Config;
use App\DataObjects\RegisterUserData;
use App\Enum\AppEnvironment;
use App\Exceptions\UnauthorizedException;
use App\Exceptions\ValidationException;
use App\ResponseFormatter;
use App\Services\AuthenticationService;
use App\Services\TokenService;
use App\Services\UserProviderService;
use Exception;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

readonly class AuthController
{
    public function __construct(
        private ResponseFormatter $responseFormatter,
        private Config $config,
        private AuthenticationService $authenticationService,
        private UserProviderService $userProviderService,
        private TokenService $tokenService
    ) {
    }

    /**
     * @param Request $request
     * @param Response $response
     *
     * @throws Exception
     *
     * @return Response
     */
    public function logIn(Request $request, Response $response): Response
    {
        $data = (array)$request->getParsedBody();

        $user = $this->userProviderService->getByCredentials($data);

        if (!$user || !$this->authenticationService->attemptLogin($user, $data)) {
            throw new ValidationException(['password' => ['You have entered an invalid email or password']]);
        }

        [$jwtToken, $refreshToken] = $this->tokenService->generateTokens($user);

        $response = $this->addRefreshTokenCookie($response, $refreshToken);

        return $this->responseFormatter->asJson($response, [
            'accessToken'  => $jwtToken,
            'refreshToken' => $refreshToken,
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     *
     * @throws Exception
     *
     * @return Response
     */
    public function register(Request $request, Response $response): Response
    {
        $data = (array)$request->getParsedBody();

        $user = $this->userProviderService->createUser(
            new RegisterUserData($data['name'], $data['email'], $data['password'])
        );

        [$jwtToken, $refreshToken] = $this->tokenService->generateTokens($user);

        $response = $this->addRefreshTokenCookie($response, $refreshToken);

        return $this->responseFormatter->asJson($response, [
            'accessToken'  => $jwtToken,
            'refreshToken' => $refreshToken,
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     *
     * @throws Exception
     *
     * @return Response
     */
    public function tokenRenewal(Request $request, Response $response): Response
    {
        $refreshToken = $request->getHeader('Refresh-Token')[0];

        $user         = $request->getAttribute('user');
        $refreshToken = $this->tokenService->getRefreshToken($user, $refreshToken);

        if (!$refreshToken) {
            throw new UnauthorizedException('Not a valid refresh token.');
        }

        $this->tokenService->removeRefreshToken($refreshToken);
        [$jwtToken, $refreshToken] = $this->tokenService->generateTokens($user);

        $response = $this->addRefreshTokenCookie($response, $refreshToken);

        return $this->responseFormatter->asJson($response, [
            'accessToken'  => $jwtToken,
            'refreshToken' => $refreshToken,
        ]);
    }

    /**
     * @param Response $response
     * @param string $refreshToken
     *
     * @return Response
     */
    private function addRefreshTokenCookie(Response $response, string $refreshToken): Response
    {
        $environment   = $this->config->get('environment');
        $isDevelopment = AppEnvironment::isDevelopment($environment);
        $expires       = time() + $this->config->get('jwt_authentication.cookie_expiration_time');
        $secure        = $isDevelopment ? '' : 'secure;';
        $sameSite      = $isDevelopment ? '' : 'SameSite=None;';
        $httpOnly      = $isDevelopment ? '' : 'HttpOnly;';

        return $response->withAddedHeader(
            'Set-Cookie',
            'refresh_token=' . $refreshToken . '; ' . $httpOnly . $secure . $sameSite . ' expires=' . gmdate('D, d M Y H:i:s T', $expires) . '; path=/'
        );
    }
}
