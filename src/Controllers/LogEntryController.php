<?php

declare(strict_types = 1);

namespace App\Controllers;

use App\Entity\LogEntry;
use App\Interfaces\IEntityManagerService;
use App\ResponseFormatter;
use App\Services\LogEntryService;
use App\Services\RequestService;
use Doctrine\ORM\EntityRepository;
use Exception;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

readonly class LogEntryController
{
    /**
     * @var EntityRepository<LogEntry>
     */
    protected EntityRepository $repository;

    public function __construct(
        private LogEntryService $logEntryService,
        private ResponseFormatter $responseFormatter,
        private IEntityManagerService $entityManagerService,
        private RequestService $requestService
    ) {
        $objectRepository = $this->entityManagerService->getRepository(LogEntry::class);
        assert($objectRepository instanceof EntityRepository);
        $this->repository = $objectRepository;
    }

    /**
     * Retrieve all items from the repository and format the response as JSON.
     *
     * @param Response $response the response object
     *
     * @return Response the formatted response object as JSON
     */
    public function getAll(Response $response): Response
    {
        $items = $this->repository->findAll();

        return $this->responseFormatter->asJson($response, $items);
    }

    /**
     * Retrieve all items from the repository and format the response as JSON.
     *
     * @param Request $request
     * @param Response $response the response object
     *
     * @throws Exception
     *
     * @return Response the formatted response object as JSON
     */
    public function getPaginated(Request $request, Response $response): Response
    {
        $params      = $this->requestService->getDataTableQueryParameters($request);
        $logs        = $this->logEntryService->getPaginatedResults($params);
        $transformer = function (LogEntry $logEntry) {
            return $logEntry;
        };

        $total = count($logs);

        return $this->responseFormatter->asDataTable(
            $response,
            array_map($transformer, (array)$logs->getIterator()),
            $params->draw,
            $total
        );
    }

    /**
     * Get logEntry.
     *
     * Returns a response after formatting it as JSON using the provided response and logEntry.
     *
     * @param Response    $response    the response object to format as JSON
     * @param LogEntry $logEntry the logEntry object to use while formatting the response
     *
     * @return Response the formatted response object
     */
    public function get(Response $response, LogEntry $logEntry): Response
    {
        return $this->responseFormatter->asJson($response, $logEntry);
    }
}
