<?php

declare(strict_types = 1);

namespace App\Validators;

use App\Interfaces\IRequestValidator;
use Cake\Validation\Validator;
use Psr\Http\Message\RequestInterface;

class LoginUserRequestValidator implements IRequestValidator
{
    protected Validator $validator;

    public function __construct()
    {
        $this->validator = new Validator();
    }

    public function setup(?RequestInterface $request = null): Validator
    {
        return $this->validator
            ->requirePresence(['email', 'password'])
            ->email('email');
    }
}
