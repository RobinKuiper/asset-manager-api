<?php

declare(strict_types = 1);

namespace App\Validators;

use App\Entity\Role;
use App\Entity\User;
use App\Interfaces\IEntityManagerService;
use App\Interfaces\IRequestValidator;
use Cake\Validation\Validator;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Psr\Http\Message\RequestInterface;

class UserRequestValidator implements IRequestValidator
{
    /**
     * @var EntityRepository<User>
     */
    protected EntityRepository $repository;
    protected Validator $validator;

    public function __construct(
        protected IEntityManagerService $entityManagerService,
    ) {
        $this->validator = new Validator();
    }

    public function setup(?RequestInterface $request = null): Validator
    {
        $objectRepository = $this->entityManagerService->getRepository(User::class);
        assert($objectRepository instanceof EntityRepository);
        $this->repository = $objectRepository;

        $validator = $this->validator
            ->minLength('name', DEFAULT_NAME_MINIMAL_LENGTH)
            ->email('email')
            ->add('name', 'custom', [
                'rule'    => [$this, 'isUniqueName'],
                'message' => 'A user with this name already exists',
            ])
            ->add('email', 'custom', [
                'rule'    => [$this, 'isUniqueEmail'],
                'message' => 'A user with this email already exists',
            ])
            ->add('roleId', 'custom', [
                'rule'    => [$this, 'isExistingRole'],
                'message' => 'The requested role does not exist',
            ]);

        if ($request && ($request->getMethod() === 'POST' || $request->getMethod() === 'PUT')) {
            $validator = $this->setCreationRules($validator);
        }

        return $validator;
    }

    /**
     * Must be public to be accessed by the validator.
     *
     * @param Validator $v
     *
     * @return Validator
     */
    private function setCreationRules(Validator $v): Validator
    {
        return $v->requirePresence(['name', 'email']);
    }

    /**
     * @param mixed $value
     * @param array $context - [data, providers, newRecord]
     *
     * @return bool
     */
    public function isUniqueName(mixed $value, array $context): bool
    {
        $data      = $context['data'];
        $arguments = $data['arguments'];

        $criteria = Criteria::create()->where(Criteria::expr()->eq('name', $value));

        if (isset($arguments['user'])) {
            $criteria->andWhere(Criteria::expr()->not(Criteria::expr()->eq('id', $arguments['user'])));
        }

        return !$this->repository->matching($criteria)->count();
    }

    /**
     * @param mixed $value
     * @param array $context - [data, providers, newRecord]
     *
     * @return bool
     */
    public function isUniqueEmail(mixed $value, array $context): bool
    {
        $data      = $context['data'];
        $arguments = $data['arguments'];

        $criteria = Criteria::create()->where(Criteria::expr()->eq('email', $value));

        if (isset($arguments['user'])) {
            $criteria->andWhere(Criteria::expr()->not(Criteria::expr()->eq('id', $arguments['user'])));
        }

        return !$this->repository->matching($criteria)->count();
    }

    /**
     * @param mixed $value
     *
     * @return bool
     */
    public function isExistingRole(mixed $value): bool
    {
        $role = $this->entityManagerService->find(Role::class, $value);

        return (bool)$role;
    }
}
