<?php

declare(strict_types = 1);

namespace App\Validators;

use App\Entity\Permission;
use App\Interfaces\IEntityManagerService;
use App\Interfaces\IRequestValidator;
use Cake\Validation\Validator;
use Psr\Http\Message\RequestInterface;

class AddPermissionRequestValidator implements IRequestValidator
{
    protected Validator $validator;

    public function __construct(
        protected IEntityManagerService $entityManagerService,
    ) {
        $this->validator = new Validator();
    }

    public function setup(?RequestInterface $request = null): Validator
    {
        return $this->validator
            ->requirePresence('permissionId')
            ->add('permissionId', 'custom', [
                'rule'    => [$this, 'isExistingPermission'],
                'message' => 'The requested permission does not exist',
            ]);
    }

    /**
     * @param mixed $value
     *
     * @return bool
     */
    public function isExistingPermission(mixed $value): bool
    {
        $permission = $this->entityManagerService->find(Permission::class, $value);

        return (bool)$permission;
    }
}
