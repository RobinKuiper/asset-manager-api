<?php

declare(strict_types = 1);

namespace App\Validators;

use App\Entity\Permission;
use App\Interfaces\IEntityManagerService;
use App\Interfaces\IRequestValidator;
use Cake\Validation\Validator;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Psr\Http\Message\RequestInterface;

class PermissionRequestValidator implements IRequestValidator
{
    /**
     * @var EntityRepository<Permission>
     */
    protected EntityRepository $repository;
    protected Validator $validator;

    public function __construct(
        protected IEntityManagerService $entityManagerService,
    ) {
        $this->validator = new Validator();
    }

    public function setup(?RequestInterface $request = null): Validator
    {
        $objectRepository = $this->entityManagerService->getRepository(Permission::class);
        assert($objectRepository instanceof EntityRepository);
        $this->repository = $objectRepository;

        $validator = $this->validator
            ->minLength('name', DEFAULT_NAME_MINIMAL_LENGTH)
            ->minLength('description', DEFAULT_DESCRIPTION_MINIMAL_LENGTH)
            ->add('name', 'custom', [
                'rule'    => [$this, 'isUniqueName'],
                'message' => 'A permission with this name already exists',
            ]);

        if ($request && ($request->getMethod() === 'POST' || $request->getMethod() === 'PUT')) {
            $validator = $this->setCreationRules($validator);
        }

        return $validator;
    }

    /**
     * Must be public to be accessed by the validator.
     *
     * @param Validator $v
     *
     * @return Validator
     */
    private function setCreationRules(Validator $v): Validator
    {
        return $v->requirePresence(['name', 'description']);
    }

    /**
     * @param mixed $value
     * @param array $context - [data, providers, newRecord]
     *
     * @return bool
     */
    public function isUniqueName(mixed $value, array $context): bool
    {
        $data      = $context['data'];
        $arguments = $data['arguments'];

        $criteria = Criteria::create()->where(Criteria::expr()->eq('name', $value));

        if (isset($arguments['permission'])) {
            $criteria->andWhere(Criteria::expr()->not(Criteria::expr()->eq('id', $arguments['permission'])));
        }

        return !$this->repository->matching($criteria)->count();
    }
}
