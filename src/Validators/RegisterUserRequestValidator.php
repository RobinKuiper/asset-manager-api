<?php

declare(strict_types = 1);

namespace App\Validators;

use App\Entity\User;
use App\Interfaces\IEntityManagerService;
use App\Interfaces\IRequestValidator;
use Cake\Validation\Validator;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Psr\Http\Message\RequestInterface;

class RegisterUserRequestValidator implements IRequestValidator
{
    /**
     * @var EntityRepository<User>
     */
    protected EntityRepository $repository;
    protected Validator $validator;

    public function __construct(
        protected IEntityManagerService $entityManagerService,
    ) {
        $this->validator = new Validator();
    }

    public function setup(?RequestInterface $request = null): Validator
    {
        $objectRepository = $this->entityManagerService->getRepository(User::class);
        assert($objectRepository instanceof EntityRepository);
        $this->repository = $objectRepository;

        return $this->validator
            ->requirePresence(['name', 'email', 'password', 'confirmPassword'])
            ->minLength('name', DEFAULT_NAME_MINIMAL_LENGTH)
            ->minLength('password', DEFAULT_PASSWORD_LENGTH)
            ->sameAs('password', 'confirmPassword')
            ->email('email')
            ->add('name', 'custom', [
                'rule'    => [$this, 'isUniqueName'],
                'message' => 'A user with this name already exists',
            ])
            ->add('email', 'custom', [
                'rule'    => [$this, 'isUniqueEmail'],
                'message' => 'A user with this email already exists',
            ]);
    }

    /**
     * @param mixed $value
     * @param array $context - [data, providers, newRecord]
     *
     * @return bool
     */
    public function isUniqueName(mixed $value, array $context): bool
    {
        $data      = $context['data'];
        $arguments = $data['arguments'];

        $criteria = Criteria::create()->where(Criteria::expr()->eq('name', $value));

        if (isset($arguments['user'])) {
            $criteria->andWhere(Criteria::expr()->not(Criteria::expr()->eq('id', $arguments['user'])));
        }

        return !$this->repository->matching($criteria)->count();
    }

    /**
     * @param mixed $value
     * @param array $context - [data, providers, newRecord]
     *
     * @return bool
     */
    public function isUniqueEmail(mixed $value, array $context): bool
    {
        $data      = $context['data'];
        $arguments = $data['arguments'];

        $criteria = Criteria::create()->where(Criteria::expr()->eq('email', $value));

        if (isset($arguments['user'])) {
            $criteria->andWhere(Criteria::expr()->not(Criteria::expr()->eq('id', $arguments['user'])));
        }

        return !$this->repository->matching($criteria)->count();
    }
}
