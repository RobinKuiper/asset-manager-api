<?php

declare(strict_types = 1);

namespace App\Command;

use App\Config;
use Exception;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class GenerateJwtSecretKeyCommand extends Command
{
    protected static $defaultName        = 'app:generate:jwt-secret';
    protected static $defaultDescription = 'Generates a new JWT_SECRET key';

    public function __construct(private readonly Config $config)
    {
        parent::__construct();
    }

    /**
     * Executes the method to generate a new JWT_SECRET and save it in the .env file.
     *
     * @param InputInterface $input the input interface
     * @param OutputInterface $output the output interface
     *
     * @throws Exception
     *
     * @return int the command status
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $hasKey = $this->config->get('jwt_authentication.secret');

        if ($hasKey) {
            /** @var QuestionHelper $helper */
            $helper = $this->getHelper('question');

            $question = new ConfirmationQuestion(
                '<question>Generating a new JWT_SECRET will invalidate any sessions associated with the old key. Are you sure you want to proceed? (y/N)</question>',
                false
            );

            if (!$helper->ask($input, $output, $question)) {
                return Command::SUCCESS;
            }
        }

        $key = base64_encode(random_bytes(DEFAULT_BYTES_LENGTH));

        $envFilePath = __DIR__ . '/../../.env';

        if (!file_exists($envFilePath)) {
            throw new RuntimeException('.env file not found');
        }

        $envFileContent = (string)file_get_contents($envFilePath);

        $pattern = '/^JWT_SECRET=.*/m';

        if (preg_match($pattern, $envFileContent)) {
            $envFileContent = preg_replace($pattern, 'JWT_SECRET=' . $key, $envFileContent);
        } else {
            $envFileContent .= PHP_EOL . 'JWT_SECRET=' . $key;
        }

        file_put_contents($envFilePath, $envFileContent);

        $output->writeln('<info>New JWT_SECRET has been generated & saved</info>');

        return Command::SUCCESS;
    }
}
