<?php

declare(strict_types = 1);

namespace App\Command;

use DI\NotFoundException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddAdminUser extends AddUser
{
    protected static $defaultName        = 'app:add:admin';
    protected static $defaultDescription = 'Add an admin user to the database';

    /**
     * Execute the command.
     *
     * @param InputInterface $input the input interface instance
     * @param OutputInterface $output the output interface instance
     *
     * @throws NotFoundException
     *
     * @return int returns the command execution status (0 for success or an error code)
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('<comment>Set up a new admin user</comment>');
        $name  = $this->getUsername($input, $output);
        $email = $this->getEmail($input, $output);

        $password       = $this->hashService->generatePassword();
        $hashedPassword = $this->hashService->hashPassword($password);

        if (!$role = $this->roleRepository->findOneBy(['name' => 'admin'])) {
            throw new NotFoundException('Role not found');
        }

        $this->addUser($name, $email, $hashedPassword, $role);

        $output->writeln("<info>New administrator `$name` has been created</info>");
        $output->writeln('Password: <info>' . $password . '</info>');

        return Command::SUCCESS;
    }
}
