<?php

declare(strict_types = 1);

namespace App\Middleware;

use App\Config;
use App\Exceptions\UnauthorizedException;
use App\Services\UserProviderService;
use DomainException;
use Exception;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Firebase\JWT\SignatureInvalidException;
use Gedmo\Blameable\BlameableListener;
use Gedmo\Loggable\LoggableListener;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Routing\RouteContext;
use UnexpectedValueException;

readonly class AuthMiddleware implements MiddlewareInterface
{
    public function __construct(
        private UserProviderService $userProvider,
        private Config $config,
        private BlameableListener $blameableListener,
        private LoggableListener $loggableListener
    ) {
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $cookies = $request->getCookieParams();

        if ($request->hasHeader('Authorization')) {
            $header = $request->getHeader('Authorization');

            if (empty($header)) {
                throw new UnauthorizedException('You have no access due to an invalid JWT token');
            }

            $bearer = trim($header[0]);
            preg_match("/Bearer\s(\S+)/", $bearer, $matches);
            $token  = $matches[1];
        } elseif (!empty($cookies['token'])) {
            $token = $cookies['token'];
        } else {
            throw new UnauthorizedException('You have no access due to an invalid JWT token');
        }

        try {
            $secret    = $this->config->get('jwt_authentication.secret');
            $algorithm = $this->config->get('jwt_authentication.algorithm');
            $key       = new Key($secret, $algorithm);
            $dataToken = JWT::decode($token, $key);
        } catch (ExpiredException $e) {
            $routeContext = RouteContext::fromRequest($request);

            if (!$request->hasHeader('Refresh-Token') || $routeContext->getRoute() && $routeContext->getRoute()->getName() !== 'tokenRenewal') {
                throw new UnauthorizedException('You have no access due to an expired JWT token');
            }

            $dataToken = $e->getPayload();
        } catch (InvalidArgumentException|DomainException|Exception|BeforeValidException|SignatureInvalidException|UnexpectedValueException $e) {
            throw new UnauthorizedException('You have no access due to an invalid JWT token');
        }

        if (isset($dataToken->user)) {
            $user = $this->userProvider->getByCredentials(['email' => $dataToken->user->email]);
        } else {
            throw new UnauthorizedException('Invalid JWT token structure');
        }
        $request = $request->withAttribute('user', $user);
        $this->blameableListener->setUserValue($user);
        $this->loggableListener->setUsername($user?->getName());

        return $handler->handle($request);
    }
}
