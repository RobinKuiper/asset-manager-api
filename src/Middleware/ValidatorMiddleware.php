<?php

declare(strict_types = 1);

namespace App\Middleware;

use App\Exceptions\ValidationException;
use App\Interfaces\IRequestValidator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Selective\Validation\Factory\CakeValidationFactory;
use Selective\Validation\ValidationError;
use Slim\Routing\RouteContext;

class ValidatorMiddleware implements MiddlewareInterface
{
    protected IRequestValidator $validator;

    public function __construct(IRequestValidator $validator)
    {
        $this->validator = $validator;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $routeContext      = RouteContext::fromRequest($request);
        $route             = $routeContext->getRoute();
        $data              = (array)$request->getParsedBody();

        if (!is_null($route)) {
            $data['arguments'] = [...$route->getArguments()];
        }

        $errors = $this->validator->setup($request)->validate($data);

        $validationFactory = new CakeValidationFactory();
        $validationResults = $validationFactory->createValidationResult($errors);

        if ($validationResults->fails()) {
            throw new ValidationException(array_reduce($validationResults->getErrors(), function ($groups, ValidationError $error) {
                $groups[$error->getField()][] = $error->getMessage();

                return $groups;
            }, []), 'Validation Error(s)');
        }

        return $handler->handle($request);
    }
}
