<?php

declare(strict_types = 1);

namespace App\Middleware;

use App\Entity\Permission;
use App\Entity\User;
use App\Exceptions\UnauthorizedException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class PermissionMiddleware implements MiddlewareInterface
{
    protected array $requiredPermissions;

    public function __construct(array $requiredPermissions)
    {
        $this->requiredPermissions = $requiredPermissions;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        /** @var User $user */
        $user = $request->getAttribute('user');

        $userPermissionNames = $user->getPermissions()->map(fn (Permission $permission) => $permission->getName());
        $rolePermissionNames = $user->getRole()->getPermissions()->map(fn (Permission $permission) => $permission->getName());

        $activePermissions = array_merge($userPermissionNames->toArray(), $rolePermissionNames->toArray());

        $intersection = array_intersect($activePermissions, $this->requiredPermissions);

        if (empty($intersection)) {
            // User does not have the required permission, throw unauthorized exception
            throw new UnauthorizedException("You don't have the required permissions");
        }

        return $handler->handle($request);
    }
}
