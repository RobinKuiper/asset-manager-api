<?php

declare(strict_types = 1);

namespace App\Interfaces;

interface IUserProviderService
{
    public function getById(int $userId): ?IUser;

    public function getByCredentials(array $credentials): ?IUser;

    public function updatePassword(IUser $user, string $password): IUser;
}
