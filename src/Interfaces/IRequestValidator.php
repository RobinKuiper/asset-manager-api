<?php

declare(strict_types = 1);

namespace App\Interfaces;

use Cake\Validation\Validator;
use Psr\Http\Message\RequestInterface;

interface IRequestValidator
{
    public function setup(?RequestInterface $request = null): Validator;
}
