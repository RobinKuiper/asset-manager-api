<?php

namespace Tests\unit;

use Fig\Http\Message\StatusCodeInterface;
use PHPUnit\Framework\TestCase;
use Tests\Traits\AppTestTrait;

/**
 * Test.
 */
class HttpTest extends TestCase
{
    use AppTestTrait;

    public function testAction(): void
    {
        $request = $this->createRequest('GET', '/');
        $response = $this->app->handle($request);

        $this->assertSame(StatusCodeInterface::STATUS_OK, $response->getStatusCode());
        $this->assertResponseContains($response, 'Nothing here!');
    }

    public function testPageNotFound(): void
    {
        $request = $this->createRequest('GET', '/not-found-page');
        $response = $this->app->handle($request);

        $this->assertSame(StatusCodeInterface::STATUS_NOT_FOUND, $response->getStatusCode());
    }
}
