<?php

declare(strict_types = 1);

namespace Tests\unit\Validators;

use App\Entity\Permission;
use App\Interfaces\IEntityManagerService;
use App\Validators\AddPermissionRequestValidator;
use PHPUnit\Framework\TestCase;
use Tests\Mocks\IEntityManagerServiceMock;

class AddPermissionRequestValidatorTest extends TestCase
{
    protected function setUp(): void
    {
    }

    public function testRequiredPermissionId(): void
    {
        $validator = (new AddPermissionRequestValidator(
            $this->createMock(IEntityManagerService::class)
        ))->setup();

        $errors = $validator->validate([]);

        $this->assertArrayHasKey('permissionId', $errors);
        $this->assertEquals('This field is required', $errors['permissionId']['_required']);
    }

    public function testNonExistingPermission(): void
    {
        $entityManager = $this->createMock(IEntityManagerServiceMock::class);
        $entityManager->expects($this->once())
            ->method('find')
            ->with(Permission::class, 123) // Replace with a non-existent ID
            ->willReturn(null);

        $validator = (new AddPermissionRequestValidator($entityManager))->setup();

        $errors = $validator->validate(['permissionId' => 123]);

        $this->assertArrayHasKey('permissionId', $errors);
        $this->assertEquals('The requested permission does not exist', $errors['permissionId']['custom']);
    }

    public function testValidPermission(): void
    {
        $entityManager = $this->createMock(IEntityManagerServiceMock::class);
        $entityManager->expects($this->once())
            ->method('find')
            ->with(Permission::class, 123) // Replace with an existing ID
            ->willReturn($this->createMock(Permission::class));

        $validator = (new AddPermissionRequestValidator($entityManager))->setup();

        $errors = $validator->validate(['permissionId' => 123]);

        $this->assertEmpty($errors);
    }
}
