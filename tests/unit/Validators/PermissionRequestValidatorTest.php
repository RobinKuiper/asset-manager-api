<?php

declare(strict_types = 1);

namespace Tests\unit\Validators;

use App\Entity\Permission;
use App\Validators\PermissionRequestValidator;
use Cake\Validation\Validator;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Tests\Traits\AppTestTrait;

class PermissionRequestValidatorTest extends TestCase
{
    use AppTestTrait;

    public function createValidator(): Validator
    {
        $request = $this->createMock(RequestInterface::class);
        $request->method('getMethod')->willReturn('POST');

        $permission = new Permission();
        $permission->setName('test.test');
        $permission->setDescription('Test permission');

        $this->entityRepository->expects($this->any())
            ->method('find')
            ->willReturn($permission);

        return (new PermissionRequestValidator(
            $this->entityManager
        ))->setup($request);
    }

    public function testRequiredFieldsForCreateOrUpdate(): void
    {
        $this->entityRepository->expects($this->any())
            ->method('matching')
            ->willReturn(new ArrayCollection([]));

        $validator = $this->createValidator();

        $errors = $validator->validate([]);

        $this->assertArrayHasKey('name', $errors);
        $this->assertEquals('This field is required', $errors['name']['_required']);

        $this->assertArrayHasKey('description', $errors);
        $this->assertEquals('This field is required', $errors['description']['_required']);
    }

    public function testMinLength(): void
    {
        $this->entityRepository->expects($this->any())
            ->method('matching')
            ->willReturn(new ArrayCollection([]));

        $validator = $this->createValidator();

        $errors = $validator->validate([
            'name' => 'na',
            'description' => 'shor',
        ]);

        $this->assertArrayHasKey('name', $errors);
        $this->assertStringContainsString('be at least', $errors['name']['minLength']);

        $this->assertArrayHasKey('description', $errors);
        $this->assertStringContainsString('be at least', $errors['description']['minLength']);
    }

    public function testUniqueNameNewPermission(): void
    {
        $this->entityRepository->expects($this->any())
            ->method('matching')
            ->willReturn(new ArrayCollection([]));

        $validator = $this->createValidator();

        $errors = $validator->validate([
            'name' => 'uniqueName',
            'description' => 'Some description',
        ]);

        $this->assertEmpty($errors);
    }

    public function testNonUniqueNameExistingPermission(): void
    {
        $this->entityRepository->expects($this->any())
            ->method('matching')
            ->willReturn(new ArrayCollection([
                'name' => 'uniqueName',
                'description' => 'Some description',
            ]));

        $validator = $this->createValidator();

        $errors = $validator->validate([
            'name' => 'uniqueName',
            'description' => 'Some description',
        ]);

        $this->assertArrayHasKey('name', $errors);
        $this->assertEquals('A permission with this name already exists', $errors['name']['custom']);
    }
}
