<?php

declare(strict_types = 1);

namespace Tests\unit\Validators;

use App\Entity\Role;
use App\Validators\RoleRequestValidator;
use Cake\Validation\Validator;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Tests\Traits\AppTestTrait;

class RoleRequestValidatorTest extends TestCase
{
    use AppTestTrait;

    public function createValidator(): Validator
    {
        $request = $this->createMock(RequestInterface::class);
        $request->method('getMethod')->willReturn('POST');

        $role = new Role();
        $role->setName('test_role');

        $this->entityRepository->expects($this->any())
            ->method('find')
            ->willReturn($role);

        return (new RoleRequestValidator(
            $this->entityManager
        ))->setup($request);
    }

    public function testRequiredFieldsForCreateOrUpdate(): void
    {
        $this->entityRepository->expects($this->any())
            ->method('matching')
            ->willReturn(new ArrayCollection([]));

        $validator = $this->createValidator();

        $errors = $validator->validate([]);

        $this->assertArrayHasKey('name', $errors);
        $this->assertEquals('This field is required', $errors['name']['_required']);
    }

    public function testMinLength(): void
    {
        $this->entityRepository->expects($this->any())
            ->method('matching')
            ->willReturn(new ArrayCollection([]));

        $validator = $this->createValidator();

        $errors = $validator->validate([
            'name' => 'na',
        ]);

        $this->assertArrayHasKey('name', $errors);
        $this->assertStringContainsString('be at least', $errors['name']['minLength']);
    }

    public function testUniqueNameNewRole(): void
    {
        $this->entityRepository->expects($this->any())
            ->method('matching')
            ->willReturn(new ArrayCollection([]));

        $validator = $this->createValidator();

        $errors = $validator->validate([
            'name' => 'uniqueName',
        ]);

        $this->assertEmpty($errors);
    }

    public function testNonUniqueNameExistingRole(): void
    {
        $this->entityRepository->expects($this->any())
            ->method('matching')
            ->willReturn(new ArrayCollection([
                'name' => 'uniqueName',
            ]));

        $validator = $this->createValidator();

        $errors = $validator->validate([
            'name' => 'uniqueName',
        ]);

        $this->assertArrayHasKey('name', $errors);
        $this->assertEquals('A role with this name already exists', $errors['name']['custom']);
    }
}
