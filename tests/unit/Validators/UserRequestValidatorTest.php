<?php

declare(strict_types = 1);

namespace Tests\unit\Validators;

use App\Entity\User;
use App\Validators\UserRequestValidator;
use Cake\Validation\Validator;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Tests\Traits\AppTestTrait;

class UserRequestValidatorTest extends TestCase
{
    use AppTestTrait;

    public function createValidator(): Validator
    {
        $request = $this->createMock(RequestInterface::class);
        $request->method('getMethod')->willReturn('POST');

        $user = new User();
        $user->setName('test_user');
        $user->setEmail('test@example.com');

        $this->entityRepository->expects($this->any())
            ->method('find')
            ->willReturn($user);

        return (new UserRequestValidator(
            $this->entityManager
        ))->setup($request);
    }

    public function testRequiredFieldsForCreateOrUpdate(): void
    {
        $this->entityRepository->expects($this->any())
            ->method('matching')
            ->willReturn(new ArrayCollection([]));

        $validator = $this->createValidator();

        $errors = $validator->validate([]);

        $this->assertArrayHasKey('name', $errors);
        $this->assertEquals('This field is required', $errors['name']['_required']);

        $this->assertArrayHasKey('email', $errors);
        $this->assertEquals('This field is required', $errors['email']['_required']);
    }

    public function testMinLength(): void
    {
        $this->entityRepository->expects($this->any())
            ->method('matching')
            ->willReturn(new ArrayCollection([]));

        $validator = $this->createValidator();

        $errors = $validator->validate([
            'name' => 'na',
            'email' => 'test@example.com',
        ]);

        $this->assertArrayHasKey('name', $errors);
        $this->assertStringContainsString('be at least', $errors['name']['minLength']);
    }

    public function testInvalidEmailFormat(): void
    {
        $this->entityRepository->expects($this->any())
            ->method('matching')
            ->willReturn(new ArrayCollection([]));

        $validator = $this->createValidator();

        $errors = $validator->validate([
            'name' => 'John Doe',
            'email' => 'invalidEmail',
        ]);

        $this->assertArrayHasKey('email', $errors);
        $this->assertEquals('The provided value must be an e-mail address', $errors['email']['email']);
    }

    public function testUniqueNameNewUser(): void
    {
        $this->entityRepository->expects($this->any())
            ->method('matching')
            ->willReturn(new ArrayCollection([]));

        $validator = $this->createValidator();

        $errors = $validator->validate([
            'name' => 'uniqueName',
            'email' => 'test@example.com',
        ]);

        $this->assertEmpty($errors);
    }

    public function testNonUniqueNameExistingUser(): void
    {
        $this->entityRepository->expects($this->any())
            ->method('matching')
            ->willReturn(new ArrayCollection([
                'name' => 'uniqueName',
                'email' => 'test@example.com',
            ]));

        $validator = $this->createValidator();

        $errors = $validator->validate([
            'name' => 'uniqueName',
            'email' => 'test@example.com',
        ]);

        $this->assertArrayHasKey('name', $errors);
        $this->assertEquals('A user with this name already exists', $errors['name']['custom']);

        $this->assertArrayHasKey('email', $errors);
        $this->assertEquals('A user with this email already exists', $errors['email']['custom']);
    }
}
