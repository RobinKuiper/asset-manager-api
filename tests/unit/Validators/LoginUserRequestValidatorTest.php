<?php

declare(strict_types = 1);

namespace Tests\unit\Validators;

use App\Validators\LoginUserRequestValidator;
use PHPUnit\Framework\TestCase;

class LoginUserRequestValidatorTest extends TestCase
{
    public function testRequiredFields(): void
    {
        $validator = (new LoginUserRequestValidator())->setup();

        $errors = $validator->validate([]);

        $this->assertArrayHasKey('email', $errors);
        $this->assertEquals('This field is required', $errors['email']['_required']);

        $this->assertArrayHasKey('password', $errors);
        $this->assertEquals('This field is required', $errors['password']['_required']);
    }

    public function testMissingEmail(): void
    {
        $validator = (new LoginUserRequestValidator())->setup();

        $errors = $validator->validate(['password' => 'secret123']);

        $this->assertArrayHasKey('email', $errors);
        $this->assertEquals('This field is required', $errors['email']['_required']);

        // Password should be valid with a missing email
        $this->assertArrayNotHasKey('password', $errors);
    }

    public function testMissingPassword(): void
    {
        $validator = (new LoginUserRequestValidator())->setup();

        $errors = $validator->validate(['email' => 'user@example.com']);

        $this->assertArrayHasKey('password', $errors);
        $this->assertEquals('This field is required', $errors['password']['_required']);

        // Email should be valid with a missing password
        $this->assertArrayNotHasKey('email', $errors);
    }

    public function testInvalidEmailFormat(): void
    {
        $validator = (new LoginUserRequestValidator())->setup();

        $errors = $validator->validate(['email' => 'invalidEmail', 'password' => 'secret123']);

        $this->assertArrayHasKey('email', $errors);
        $this->assertEquals('The provided value must be an e-mail address', $errors['email']['email']);
    }

    public function testValidRequest(): void
    {
        $validator = (new LoginUserRequestValidator())->setup();

        $errors = $validator->validate(['email' => 'user@example.com', 'password' => 'secret123']);

        $this->assertEmpty($errors);
    }
}
