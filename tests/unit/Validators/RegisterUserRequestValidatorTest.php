<?php

declare(strict_types = 1);

namespace Tests\unit\Validators;

use App\Validators\RegisterUserRequestValidator;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Tests\Traits\AppTestTrait;

class RegisterUserRequestValidatorTest extends TestCase
{
    use AppTestTrait;

    public function testRequiredFields(): void
    {
        $validator = (new RegisterUserRequestValidator($this->entityManager))->setup();

        $errors = $validator->validate([]);

        $this->assertArrayHasKey('name', $errors);
        $this->assertEquals('This field is required', $errors['name']['_required']);

        $this->assertArrayHasKey('email', $errors);
        $this->assertEquals('This field is required', $errors['email']['_required']);

        $this->assertArrayHasKey('password', $errors);
        $this->assertEquals('This field is required', $errors['password']['_required']);

        $this->assertArrayHasKey('confirmPassword', $errors);
        $this->assertEquals('This field is required', $errors['confirmPassword']['_required']);
    }

    public function testMinLength(): void
    {
        $this->entityRepository->expects($this->any())
            ->method('matching')
            ->willReturn(new ArrayCollection([]));

        $validator = (new RegisterUserRequestValidator($this->entityManager))->setup();

        $errors = $validator->validate([
            'name' => 'na',
            'email' => 'user@example.com',
            'password' => 'short',
            'confirmPassword' => 'short',
        ]);

        $this->assertArrayHasKey('name', $errors);
        $this->assertStringContainsString('be at least', $errors['name']['minLength']);

        $this->assertArrayHasKey('password', $errors);
        $this->assertStringContainsString('be at least', $errors['password']['minLength']);
    }

    public function testPasswordConfirmation(): void
    {
        $this->entityRepository->expects($this->any())
            ->method('matching')
            ->willReturn(new ArrayCollection([]));

        $validator = (new RegisterUserRequestValidator($this->entityManager))->setup();

        $errors = $validator->validate([
            'name' => 'John Doe',
            'email' => 'user@example.com',
            'password' => 'secret123',
            'confirmPassword' => 'differentPassword',
        ]);

        $this->assertArrayHasKey('password', $errors);
        $this->assertStringContainsString('must be same as', $errors['password']['sameAs']);
    }

    public function testUniqueNameNewUser(): void
    {
        $this->entityRepository->expects($this->any())
            ->method('matching')
            ->willReturn(new ArrayCollection([]));

        $validator = (new RegisterUserRequestValidator($this->entityManager))->setup();

        $errors = $validator->validate([
            'name' => 'uniqueName',
            'email' => 'user@example.com',
            'password' => 'secret123',
            'confirmPassword' => 'secret123',
        ]);

        $this->assertEmpty($errors);
    }

    public function testNonUniqueNameNewUser(): void
    {
        $this->entityRepository->expects($this->any())
            ->method('matching')
            ->willReturn(new ArrayCollection([
                'name' => 'uniqueName',
                'email' => 'user@example.com',
                'password' => 'secret123',
                'confirmPassword' => 'secret123',
            ]));

        $validator = (new RegisterUserRequestValidator($this->entityManager))->setup();

        $errors = $validator->validate([
            'name' => 'uniqueName',
            'email' => 'user@example.com',
            'password' => 'secret123',
            'confirmPassword' => 'secret123',
        ]);

        $this->assertArrayHasKey('name', $errors);
        $this->assertStringContainsString('A user with this name already exists', $errors['name']['custom']);
    }
}
