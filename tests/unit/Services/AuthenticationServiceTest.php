<?php

declare(strict_types = 1);

namespace Tests\unit\Services;

use App\Interfaces\IUser;
use App\Services\AuthenticationService;
use App\Services\HashService;
use PHPUnit\Framework\TestCase;

class AuthenticationServiceTest extends TestCase
{
    public function testSuccessfulLogin(): void
    {
        $hashService = new HashService();
        $password    = 'secretpassword';

        $userMock = $this->createMock(IUser::class);
        $userMock->method('getPassword')->willReturn($hashService->hashPassword($password)); // Hashed password

        $credentials = [
            'password' => $password,
        ];

        $authService = new AuthenticationService();
        $loggedIn = $authService->attemptLogin($userMock, $credentials);

        $this->assertTrue($loggedIn);
    }

    public function testIncorrectPassword(): void
    {
        $userMock = $this->createMock(IUser::class);
        $userMock->method('getPassword')->willReturn('$2y$10$JOzLnK1tzaNGRvu.tQs1TOOIuV.lQz0vJpdIuYrMNLVeaPTvl6tIu'); // Hashed password

        $credentials = [
            'password' => 'wrongpassword',
        ];

        $authService = new AuthenticationService();
        $loggedIn = $authService->attemptLogin($userMock, $credentials);

        $this->assertFalse($loggedIn);
    }
}
