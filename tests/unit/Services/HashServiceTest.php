<?php

declare(strict_types = 1);

namespace Tests\unit\Services;

use App\Services\HashService;
use PHPUnit\Framework\TestCase;

class HashServiceTest extends TestCase
{
    private HashService $hashService;

    protected function setUp(): void
    {
        $this->hashService = new HashService();
    }

    /**
     * Test the hashPassword method.
     */
    public function testHashPassword(): void
    {
        $password = 'testPassword';
        $hashedPassword = $this->hashService->hashPassword($password);
        $this->assertNotSame($password, $hashedPassword); // Ensure it's not the plain text
        $this->assertTrue(password_verify($password, $hashedPassword)); // Verify hash is valid
    }

    /**
     * Test the generatePassword method.
     */
    public function testGeneratePassword(): void
    {
        $length = 10;
        $password = $this->hashService->generatePassword($length);
        $this->assertEquals($length, strlen($password)); // Check length
        $this->assertMatchesRegularExpression('/[\w\W]{10}/', $password);
    }
}
