<?php

declare(strict_types = 1);

namespace Tests\unit\Services;

use App\Config;
use App\Entity\Role;
use App\Entity\User;
use App\Services\TokenService;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use PHPUnit\Framework\TestCase;
use Tests\Traits\AppTestTrait;

class TokenServiceTest extends TestCase
{
    use AppTestTrait;

    public function testGenerateTokens(): void
    {
        $role = new Role();
        $role->setName('User');

        $user = new User();
        $user->setEmail('test@example.com');
        $user->setName('testuser');
        $user->setRole($role);

        $service = new TokenService($this->entityManager, $this->container->get(Config::class));
        $tokens = $service->generateTokens($user);

        $this->assertCount(2, $tokens);
        $this->assertNotEmpty($tokens[0]); // JWT token
        $this->assertNotEmpty($tokens[1]); // Refresh token
    }

    public function testGenerateRefreshToken(): void
    {
        $role = new Role();
        $role->setName('User');

        $user = new User();
        $user->setEmail('test@example.com');
        $user->setName('testuser');
        $user->setRole($role);

        $service = new TokenService($this->entityManager, $this->container->get(Config::class));
        $refreshToken = $service->generateRefreshToken($user);

        $this->assertNotEmpty($refreshToken);
    }

    public function testGenerateJwtToken(): void
    {
        $role = new Role();
        $role->setName('User');

        $user = new User();
        $user->setEmail('test@example.com');
        $user->setName('testuser');
        $user->setRole($role);

        $config = $this->container->get(Config::class);
        $service = new TokenService($this->entityManager, $config);
        $jwtToken = $service->generateJwtToken($user);

        $secret    = $config->get('jwt_authentication.secret');
        $algorithm = $config->get('jwt_authentication.algorithm');
        $key       = new Key($secret, $algorithm);
        $dataToken = JWT::decode($jwtToken, $key);

        $this->assertObjectHasProperty('exp', $dataToken);
        $this->assertObjectHasProperty('iat', $dataToken);
        $this->assertObjectHasProperty('user', $dataToken);
        $this->assertObjectHasProperty('name', $dataToken->user);
        $this->assertObjectHasProperty('email', $dataToken->user);
        $this->assertObjectHasProperty('role', $dataToken->user);
    }
}
