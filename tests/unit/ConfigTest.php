<?php

declare(strict_types = 1);

namespace Tests\unit;

use App\Config;
use PHPUnit\Framework\TestCase;

class ConfigTest extends TestCase
{
    /**
     * @covers \App\Config::__construct
     * @covers \App\Config::get
     */
    public function isAbleToGetSetting(): void
    {
        $configArray = ['db' => ['host' => 'localhost', 'user' => 'root']];
        $config = new Config($configArray);

        // Test a value that exists.
        $this->assertSame('localhost', $config->get('db.host'));

        // Test a default value for a non-existing key.
        $this->assertSame('default', $config->get('nonexisting.key', 'default'));

        // Test null for a non-existing key without a default value.
        $this->assertNull($config->get('nonexisting.key'));
    }

    /** @test */
    public function itIsAbleToGetNestedSettings(): void
    {
        $config = [
            'doctrine' => [
                'connection' => [
                    'user' => 'root',
                ],
            ],
        ];

        $config = new Config($config);

        $this->assertEquals('root', $config->get('doctrine.connection.user'));
        $this->assertEquals(['user' => 'root'], $config->get('doctrine.connection'));
    }

    /** @test */
    public function itGetsTheDefaultValueWhenSettingIsNotFound(): void
    {
        $config = [
            'doctrine' => [
                'connection' => [
                    'user' => 'root',
                ],
            ],
        ];

        $config = new Config($config);

        $this->assertEquals('pdo_mysql', $config->get('doctrine.connection.driver', 'pdo_mysql'));
        $this->assertEquals('bar', $config->get('foo', 'bar'));
        $this->assertEquals('baz', $config->get('foo.bar', 'baz'));
    }

    /** @test */
    public function itReturnsNullByDefaultWhenSettingIsNotFound(): void
    {
        $config = [
            'doctrine' => [
                'connection' => [
                    'user' => 'root',
                ],
            ],
        ];

        $config = new Config($config);

        $this->assertNull($config->get('doctrine.connection.driver'));
        $this->assertNull($config->get('foo.bar'));
    }
}
