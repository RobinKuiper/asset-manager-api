<?php

namespace Tests\Traits;

use Doctrine\ORM\EntityRepository;
use Tests\Mocks\IEntityManagerServiceMock;

trait DoctrineTestTrait
{
    protected EntityRepository $entityRepository;
    protected IEntityManagerServiceMock $entityManager;

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     *
     * @return void
     */
    protected function setUpDoctrine(): void
    {
        $this->entityRepository = $this->createMock(EntityRepository::class);
        $this->entityManager    = $this->createMock(IEntityManagerServiceMock::class);
        $this->entityManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($this->entityRepository);
    }
}
