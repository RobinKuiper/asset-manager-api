<?php

declare(strict_types = 1);

namespace Tests\Mocks;

use App\Interfaces\IEntityManagerService;

interface IEntityManagerServiceMock extends IEntityManagerService
{
    public function find(string $className, mixed $id);

    public function getRepository($className);
}
